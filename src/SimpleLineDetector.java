import TI.BoeBot;

/**
 * Write a description of class SimpleLineDetector here.
 * 
 * @author Hans van der Linden, Avans Hogeschool
 * @version 2016-12-12
 * @version 2018-13-12 Migratie naar IntelliJ Diederich Kroeske
 */
public class SimpleLineDetector
{
    public enum Sensor {LEFTSENSOR, RIGHTSENSOR, SIDESENSOR}

    static final int LINESENSORADCTHRESHOLD = 99999; // ADC values above this threshold indicate black

    // TODO Define ADC channels for the sensors
    
    /**
     * Detects a black line on a white background using the QTI IR sensor
     * 
     * @param sensor number indicating the sensor (left, right, or side)
     * @return true if sensor is on a black line, false if it isn't
     */
    public boolean isLineDetected(Sensor sensor)
    {
        boolean result = false;
        switch (sensor) {
            case LEFTSENSOR:
                result = BoeBot.analogRead(LEFTADCCHAN) > LINESENSORADCTHRESHOLD;
                break;
            // TODO Complete this code
            
            default:
                System.out.println("ERROR: unknown line sensor");
                break;
        }
        return result;
    }
}

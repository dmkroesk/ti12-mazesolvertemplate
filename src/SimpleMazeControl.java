import java.awt.Color;
import TI.BoeBot;
import TI.Timer;

/**
 * Controls movement of the robot in a maze
 * Follows black lines with intersections
 * Stops and turns at intersections
 * 
 * @author Hans van der Linden, Avans Hogeschool
 * @version 2016-12-12
 * @version 2018-13-12 Migratie naar IntelliJ Diederich Kroeske
 */
public class SimpleMazeControl
{
    enum BotState {FollowingLine, TurningRight, TurningLeft}
    private BotState state;
    
    static final int STATELED = 1; // RGB LED for state
    static final int LEFTSENSORLED = 3; // RGB LED for left line detector sensor
    static final int RIGHTSENSORLED = 5; // RGB LED for right line detector sensor
    static final int SIDESENSORLED = 4; // RGB LED for side line detector sensor
    static final int IGNORELINESENSORSTATELED = 2; // RGB LED for state of ignore timers for sensors
    static final int DISTANCELED = 0; // RGB LED for ultrasonic distance state

    static final int MINIMUMFREEDISTANCE = 6; // free distance when moving, turn when wall gets closer than this
    static final int WALLDETECTIONDISTANCE = 15; // distance from ultrasonic sensor to center of next square, in cm

    private final float FORWARDSPEED = 0.2F; // move moderately quickly in the maze
    private final float STEERRATE = 0.3F; // turn sharply while steering to keep the bot on the line
    private final float TURNSPEED = 0.1F; // turn slowly in the maze to prevent overshooting the line
    static final int SIDESENSORIGNOREPERIOD = 700;  // time in milliseconds that the side line sensor will
                                                    // be ignored when moving away from an intersection
    static final int LEFTRIGHTSENSORIGNOREPERIOD = 500; // time in milliseconds that the left and right
                                                        // line sensors are ignored after starting a turn
    
    private SimpleDriveSystem driveSystem;
    private Timer ignoreSideLineSensorTimer;
    private Timer ignoreLeftRightLineSensorTimer;
    private boolean ignoreSideLineSensor;
    private boolean ignoreLeftRightLineSensor;
    private boolean previousSideLineDetected;
    private boolean leftSensorPreviouslyDetectedLine;
    private boolean rightSensorPreviouslyDetectedLine;
    
    private SimpleUltrasonicRangefinder rangeFinder;
    
    private SimpleLineDetector lineDetector;

    /**
     * Constructor for objects of class SimpleMazeControl
     */
    public SimpleMazeControl(SimpleDriveSystem driveSystem, SimpleUltrasonicRangefinder rangeFinder, SimpleLineDetector lineDetector)
    {
        this.driveSystem = driveSystem;
        this.rangeFinder = rangeFinder;
        this.lineDetector = lineDetector;
        state = BotState.FollowingLine;
        ignoreSideLineSensorTimer = new Timer(SIDESENSORIGNOREPERIOD);
        ignoreSideLineSensor = false;
        ignoreLeftRightLineSensorTimer = new Timer(LEFTRIGHTSENSORIGNOREPERIOD);
        ignoreLeftRightLineSensor = false;
        previousSideLineDetected = false;
        leftSensorPreviouslyDetectedLine = true;
        rightSensorPreviouslyDetectedLine = true;
    }
    
    /**
     * Update motion state, depending on the current position in the maze
     * This method should be called periodically from the application's main event loop
     */
    public void update()
    {
        // Perform initial measurements and show results on RGB LEDs
        showAllLineSensorStates(); // show what the line detector sensors see
        showIgnoreLineSensorState(); // show if the line detector sensors are being ignored
        showDistanceToWall(); // show if the range finder detects a wall (and if it is too close)
        // Process according to current state
        switch (state) {
            case FollowingLine:
                handleFollowingLine();
                break;
            case TurningRight:
                handleTurningRight();
                break;
            case TurningLeft:
                handleTurningLeft();
                break;
        }
    }
    
    /**
     * Follow the line, steering left or right when necessary
     * If an intersection is reached, change state
     */
    private void handleFollowingLine()
    {
        // Use line detectors to steer left or right
        if (lineIsToLeft())
            steerLeft();
        else if (lineIsToRight())
            steerRight();
        //else if (lineIsMissing())
        //    steerOpposite();
        else
            steerStraightAhead();
        // If we come to an intersection, stop the bot and change state to turn right
        if (intersectionIsReached() || isTooCloseToWall())
        {
            System.out.println("Intersection reached");
            startIgnoringLefRightLineSensors();
            turnRight();
            state = BotState.TurningRight;
            showState(state);
        }
    }
    
    /**
     * Detect if line to the right is reached (and turn to the right is completed)
     * If line is reached, change state
     */
    private void handleTurningRight()
    {
        // If we have reached the line on the right, stop the turn
        if (lineToRightIsReached())
        {
            System.out.println("Turn completed");
            stopTurning();
            // If there is a wall in front, turn to the left, else follow the line ahead
            if (isFacingWall())
            {
                startIgnoringLefRightLineSensors();
                turnLeft();
                state = BotState.TurningLeft;
                showState(state);
            }
            else
            {
                startIgnoringSideLineSensor();
                steerStraightAhead();
                state = BotState.FollowingLine;
                showState(state);
            }
        }
    }
    
    /**
     * Detect if line to the left is reached (and turn to the left is completed)
     * If line is reached, change state
     */
    private void handleTurningLeft()
    {
        // If we have reached the line on the left, stop the turn
        if (lineToLeftIsReached())
        {
            System.out.println("Turn completed");
            stopTurning();
            // If there is a wall in front, turn to the left again, else follow the line ahead
            if (isFacingWall())
            {
                System.out.println("Wall in front");
                startIgnoringLefRightLineSensors();
                turnLeft();
                // State doesn't change so no need to show it
            }
            else
            {
                System.out.println("No wall in front");
                startIgnoringSideLineSensor();
                steerStraightAhead();
                state = BotState.FollowingLine;
                showState(state);
            }
        }
    }
    
    /**
     * Let drive system steer straight ahead
     */
    private void steerStraightAhead()
    {
        // Move straight ahead using the preset speed
        driveSystem.setBotTargetSpeed(FORWARDSPEED, 0.0F);
        System.out.println("Move straight ahead with speed " + FORWARDSPEED);
    }
    
    /**
     * Let drive system steer left
     */
    private void steerLeft()
    {
        driveSystem.setBotTargetSpeed(FORWARDSPEED, -STEERRATE);
        System.out.println("Steer left with speed " + FORWARDSPEED + " and steer rate " + STEERRATE);
    }
    
    /**
     * Let drive system steer right
     */
    private void steerRight()
    {
        driveSystem.setBotTargetSpeed(FORWARDSPEED, STEERRATE);
        System.out.println("Steer right with speed " + FORWARDSPEED + " and steer rate " + STEERRATE);
    }
    
    /**
     * Let drive system turn right on the spot
     */
    private void turnRight()
    {
        driveSystem.setBotTargetSpeed(TURNSPEED, 1.0F); // turn on the spot
        System.out.println("Turn right with speed " + TURNSPEED);
    }
    
    /**
     * Let drive system turn left on the spot
     */
    private void turnLeft()
    {
        driveSystem.setBotTargetSpeed(TURNSPEED, -1.0F); // turn on the spot
        System.out.println("Turn left with speed " + TURNSPEED);
    }
    
    /**
     * Let drive system stop the robot
     */
    private void stopTurning()
    {
        driveSystem.setBotTargetSpeed(0.0F, 0.0F);
        System.out.println("Stop turning");
    }
    
    /**
     * Start a timer to ignore the intersection line sensor temporarily
     * to avoid a false detection of the same intersection that we just left
     */
    private void startIgnoringSideLineSensor()
    {
        // Ignore the side line sensor for a short period of time after moving away
        // from an intersection, to avoid identifying the current intersection as the new one
        // Set up a timer to detect the end of the ignore periode
        ignoreSideLineSensorTimer.mark();
        ignoreSideLineSensor = true;
    }
    
    /**
     * Start a timer to temporarily ignore the left and right line sensors
     */
    private void startIgnoringLefRightLineSensors()
    {
        // Ignore the left and right line sensor for a short period of time after starting a turn
        ignoreLeftRightLineSensorTimer.mark();
        ignoreLeftRightLineSensor = true;
    }
    
    /**
     * Let line detector system determine if the line is to the left of the robot
     */
    private boolean lineIsToLeft()
    {
        return lineDetector.isLineDetected(SimpleLineDetector.Sensor.LEFTSENSOR)
            && !lineDetector.isLineDetected(SimpleLineDetector.Sensor.RIGHTSENSOR);
    }
    
    /**
     * Let line detector system determine if the line is to the right of the robot
     */
    private boolean lineIsToRight()
    {
        return lineDetector.isLineDetected(SimpleLineDetector.Sensor.RIGHTSENSOR)
            && !lineDetector.isLineDetected(SimpleLineDetector.Sensor.LEFTSENSOR);
    }
    
    /**
     * Detect an intersection using the line detector system and report the result
     * Ignore the side line sensor if its ignore timer hasn't timed out yet
     * Detection works on the principle that the side line sensor has passed the line,
     * so goes from line detected to no line detected
     * 
     * @return true if an intersection is reached, false if not or if the side sensor is still being ignored
     */
    private boolean intersectionIsReached()
    {
        boolean intersectionDetected = false;
        if (ignoreSideLineSensor)
        {
            if (ignoreSideLineSensorTimer.timeout())
            {
                // Ignore period is over
                ignoreSideLineSensor = false;
            }
        }
        if (!ignoreSideLineSensor)
        {
            // Detect a line towards the side of the robot, next to the center line
            // Note that a line sensor must be positioned there
            boolean currentSideLineDetected = lineDetector.isLineDetected(SimpleLineDetector.Sensor.SIDESENSOR);
            // An intersection is reached when the side line sensor has just passed the line,
            // so that the line on the side was previously detected but is no longer detected
            intersectionDetected = previousSideLineDetected && !currentSideLineDetected;
            previousSideLineDetected = currentSideLineDetected; // store the new value for the next comparison
        }
        return intersectionDetected;
    }
    
    /**
     * While turning to the right, detect if the new line is reached
     * 
     * @return true if line is reached, false if not or if the sensors are still being ignored
     */
    private boolean lineToRightIsReached()
    {
        boolean lineReached = false;
        if (ignoreLeftRightLineSensor)
        {
            if (ignoreLeftRightLineSensorTimer.timeout())
            {
                // Ignore period is over
                ignoreLeftRightLineSensor = false;
            }
        }
        if (!ignoreLeftRightLineSensor)
        {
            // Detect the line to reach while turning right
            boolean leftSensorDetectsLine = lineDetector.isLineDetected(SimpleLineDetector.Sensor.LEFTSENSOR);
            // A line is reached when the left sensor has just detected the line,
            // but didn't detect a line previously, i.e. if the sensor moves from white floor to black line
            // This is to avoid accidentally detecting the line that the bot is on when it starts the turn
            lineReached = leftSensorDetectsLine && !leftSensorPreviouslyDetectedLine;
            leftSensorPreviouslyDetectedLine = leftSensorDetectsLine; // store the new value for the next comparison
        }
        return lineReached;
    }

    /**
     * While turning to the left, detect if the new line is reached
     * 
     * @return true if line is reached, false if not or if the sensors are still being ignored
     */
    private boolean lineToLeftIsReached()
    {
        boolean lineReached = false;
        if (ignoreLeftRightLineSensor)
        {
            if (ignoreLeftRightLineSensorTimer.timeout())
            {
                // Ignore period is over
                ignoreLeftRightLineSensor = false;
            }
        }
        if (!ignoreLeftRightLineSensor)
        {
            // Detect the line to reach while turning left
            boolean rightSensorDetectsLine = lineDetector.isLineDetected(SimpleLineDetector.Sensor.RIGHTSENSOR);
            // A line is reached when the right sensor has just detected the line,
            // but didn't detect a line previously, i.e. if the sensor moves from white floor to black line
            // This is to avoid accidentally detecting the line that the bot is on when it starts the turn
            lineReached = rightSensorDetectsLine && !rightSensorPreviouslyDetectedLine;
            rightSensorPreviouslyDetectedLine = rightSensorDetectsLine; // store the new value for the next comparison
        }
        return lineReached;
    }
    
    /**
     * Detect if the robot is facing a wall (or an open corridor)
     * 
     * @return true if the robot is facing a wall, false if it is facing an open corridor
     */
    private boolean isFacingWall()
    {
        // Determine if bot is facing a wall or facing an open corridor in the maze
        boolean wallInFront = distanceToWallBelow(WALLDETECTIONDISTANCE);
        return wallInFront;
    }
    
    /**
     * Detect if the robot has accidentally come too close to a wall
     * 
     * @return true if the robot is too close to a wall (and thus must have missed an intersection)
     */
    private boolean isTooCloseToWall()
    {
        // Determine if bot has accidentally come too close to a wall
        // (because it may have missed an intersection)
        boolean closeToWall = distanceToWallBelow(MINIMUMFREEDISTANCE);
        return closeToWall;
    }
    
    /**
     * Returns true if distance to wall is below the given threshold
     * Uses rangefinder system to measure the distance
     */
    private boolean distanceToWallBelow(int lowerLimit)
    {
        // Measure distance to nearest object in front, using the ultrasonic range finder
        int distanceToWall = rangeFinder.measureDistance();
        return distanceToWall < lowerLimit;
    }
    
    
    /**
     * Show if the robot is facing a wall, is too close, or is facing a corridor
     */
    private void showDistanceToWall()
    {
        if (isTooCloseToWall())
        {
            BoeBot.rgbSet(DISTANCELED, Color.RED);
        }
        else if (isFacingWall())
        {
            BoeBot.rgbSet(DISTANCELED, Color.YELLOW);
        }
        else
        {
            BoeBot.rgbSet(DISTANCELED, Color.BLACK);
        }
        BoeBot.rgbShow();
    }
    
    /**
     * Show all line sensor states
     */
    private void showAllLineSensorStates()
    {
        boolean sensorLeftDetectsLine = lineDetector.isLineDetected(SimpleLineDetector.Sensor.LEFTSENSOR);
        boolean sensorRightDetectsLine = lineDetector.isLineDetected(SimpleLineDetector.Sensor.RIGHTSENSOR);
        boolean sensorSideDetectsLine = lineDetector.isLineDetected(SimpleLineDetector.Sensor.SIDESENSOR);
        if (sensorLeftDetectsLine) {
            BoeBot.rgbSet(LEFTSENSORLED, Color.BLACK);
        } else {
            BoeBot.rgbSet(LEFTSENSORLED, Color.WHITE);
        }
        if (sensorRightDetectsLine) {
            BoeBot.rgbSet(RIGHTSENSORLED, Color.BLACK);
        } else {
            BoeBot.rgbSet(RIGHTSENSORLED, Color.WHITE);
        }
        if (sensorSideDetectsLine) {
            BoeBot.rgbSet(SIDESENSORLED, Color.BLACK);
        } else {
            BoeBot.rgbSet(SIDESENSORLED, Color.WHITE);
        }
        BoeBot.rgbShow();
    }
        
    private void showIgnoreLineSensorState()
    {
        if (ignoreLeftRightLineSensor) {
            BoeBot.rgbSet(IGNORELINESENSORSTATELED, Color.BLUE);
        } else if (ignoreSideLineSensor) {
            BoeBot.rgbSet(IGNORELINESENSORSTATELED, Color.RED);
        } else {
            BoeBot.rgbSet(IGNORELINESENSORSTATELED, Color.BLACK);
        }
        BoeBot.rgbShow();
    }

    //private void showState(BotState state, ColorLEDSystem leds)
    private void showState(BotState state)
    {
        switch (state) {
            case FollowingLine:
                BoeBot.rgbSet(1, Color.GREEN);
                break;
            case TurningRight:
                BoeBot.rgbSet(1, Color.RED);
                break;
            case TurningLeft:
                BoeBot.rgbSet(1, Color.WHITE);
                break;
        }
        BoeBot.rgbShow();
    }
}

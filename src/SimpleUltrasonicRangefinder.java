import TI.BoeBot;

/**
 * Simple ultrasonic range finder
 * Triggers an ultrasonic pulse and measures the time it takes for the echo to return,
 * and then calculates the distance in cm
 * 
 * @author Hans van der Linden, Avans Hogeschool
 * @version 2016-12-12
 * @version 2018-13-12 Migratie naar IntelliJ Diederich Kroeske
 */
public class SimpleUltrasonicRangefinder
{
    // TODO Define GPIO pins to use
    
    /**
     * Measure the distance to the nearest object in front of the ultrasonic range sensor
     * 
     * @return distance in cm (rounded to the nearest cm)
     */
    public int measureDistance()
    {
        // TODO Complete this code
        
    }
}

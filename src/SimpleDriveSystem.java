import TI.Servo;

/**
 * Simple drive system for the BoeBot
 * Accepts target speed and target turn rate, both in range -1.0F to 1.0F
 * Adjusts actual speed and turn rate gradually towards the target values
 * Periodically call the update() method to perform the gradual adjustments
 * 
 * @author Hans van der Linden, Avans Hogeschool 
 * @version 2016-12-12
 * @version 2018-13-12 Migratie naar IntelliJ Diederich Kroeske
 */
public class SimpleDriveSystem
{
    private final float SPEEDSTEP = 0.1F; // lower is smoother but slower
    private final float TURNRATESTEP = 0.3F; // lower is smoother but slower

    private float targetSpeed;
    private float targetTurnRate;
    private float currentSpeed;
    private float currentTurnRate;
    
    // TODO Complete this code
    
    /**
     * Constructor for objects of class SimpleDriveSystem
     */
    public SimpleDriveSystem()
    {
        // TODO Complete this code
    }

    /**
     * Set the target speed and target turn rate
     * @param speed forward (or backward) speed, where 1.0 = full forward and -1.0 = full backward
     * @param turnRate turning speed, where -1.0 = full anti clockwise and 1.0 = full clockwise
     */
    public void setBotTargetSpeed(float speed, float turnRate)
    {
        targetSpeed = speed;
        targetTurnRate = turnRate;
    }
    
    /**
     * Update the current speed and turn rate towards their target values
     * This method must be periodically called from the main event loop
     */
    public void update()
    {
        if (currentSpeed < targetSpeed)
        {
            currentSpeed += SPEEDSTEP;
            if (currentSpeed > targetSpeed) currentSpeed = targetSpeed;
        }
        else if (currentSpeed > targetSpeed)
        // TODO Complete this code
        
        setBotSpeed(currentSpeed, currentTurnRate);
    }
    
    /**
     * Sets the combined rotational speed of left and right motor,
     * based on speed and turn rate
     * Improved algorithm means that one motor is slowed down or even reversed
     * and the other remains at the set speed, so that turning is still possible
     * even at top speed
     * Note that speed must be nonzero for the motors to work, so just using a
     * turn rate > 0.0 with speed = 0.0 will not have any effect
     * 
     * @param speed     forward or backward speed, -1.0 = full backward, 0.0 = stop, 1.0 = full forward
     * @param turnRate turning speed, -1.0 = full anti-clockwise, 0.0 = forward, 1.0 = full clockwise
     */
    private void setBotSpeed(float speed, float turnRate)
    {
        // TODO Complete this code
        
        motorLeft.update(speedToPulseLength(motorLeftSpeed, false));
        motorRight.update(speedToPulseLength(motorRightSpeed, true));
    }
    
    /**
     * Maps speed in range -1.0 to 1.0 to a pulse length for the servo motor
     * where -1.0 gives the minimum pulse length and 1.0 gives the maximum pulse length
     * 
     * @param speed       the speed in range -1.0 .. 1.0
     * @param reverse     if true, -1.0 maps to max length and 1.0 maps to min length
     * @return            the pulse length in us
     */
    private int speedToPulseLength(float speed, boolean reverse)
    {
        int pulseLength = BASEPULSEWIDTH;
        if (speed > 1.0F) speed = 1.0F;
        if (speed < -1.0F) speed = -1.0F;
        if (reverse)
        {
            pulseLength -= Math.round(speed * MAXPULSEWIDTH);
        }
        else
        {
            pulseLength += Math.round(speed * MAXPULSEWIDTH);
        }
        return pulseLength;
    }
}

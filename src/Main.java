import TI.BoeBot;

/**
 * Main entry point for simple maze solver code for BoeBot
 * 
 * @author Hans van der Linden
 * @version 2016-12-12
 * @version 2018-13-12 Migratie naar IntelliJ Diederich Kroeske
 */
public class Main
{
    public static void main(String[] args)
    {
        System.out.println("Simple maze solver is running...");
        SimpleDriveSystem driveSystem = new SimpleDriveSystem();
        SimpleLineDetector lineDetector = new SimpleLineDetector();
        SimpleUltrasonicRangefinder rangeFinder = new SimpleUltrasonicRangefinder();
        SimpleMazeControl simpleMazeControl = new SimpleMazeControl(driveSystem, rangeFinder, lineDetector);
        while(true)
        {
            simpleMazeControl.update();
            driveSystem.update();
        }
    }
}
